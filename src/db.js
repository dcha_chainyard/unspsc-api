const sqlite3 = require('sqlite3').verbose()

const db = new sqlite3.Database('./unspc.sql', sqlite3.OPEN_READONLY, (err) => {
  if (err) {
    return console.error(err.message)
  }

  console.log('Connected to the unspc database')
})

module.exports = db