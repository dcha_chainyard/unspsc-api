const express = require('express')
const morgan = require('morgan')
const UNSPSC = require('./UNSPSC.json')

const app = express()

app.use(morgan('tiny'))

const SEGMENTS = UNSPSC.map(segment => ({ id: segment.Segment, name: segment.SegmentName }))

const FAMILIES = UNSPSC
  .map(segment => segment.Families)
  .flat()
  .map(family => ({ id: family.Family, name: family.FamilyName }))

const CLASSES = UNSPSC.flatMap(segment => segment.Families)
  .flatMap(family => family.Classes)
  .map(c => ({ id: c.Class, name: c.ClassName }))

const COMMODITIES = UNSPSC.flatMap(segment => segment.Families)
  .flatMap(family => family.Classes)
  .flatMap(c => c.Commodities)
  .map(commodity => ({ id: commodity.Commodity, name: commodity.CommodityName }))

class UNSPSCController {
  static getSegments(req, res) {
    res.json(SEGMENTS)
  }

  static getSegmentById(req, res) {
    const { id } = req.params
    const segment = SEGMENTS.find(s => s.id === id)

    if (!segment) {
      return res.status(400).json({ error: `No segment found for id ${id}` })
    }

    const segmentCode = id.substring(0, 2)

    // Get all families with this segmentId
    segment.families = FAMILIES.filter(family => family.id.substring(0, 2) == segmentCode)

    res.json(segment)
  }

  static getFamilyById(req, res) {
    const { id } = req.params

    // Get all classes that have the same first 4 digits as the given code
    const family = FAMILIES.find(f => f.id == id)

    if (!family) {
      return res.status(400).json({ error: `No family found for id ${id}` })
    }

    const familyCode = id.substring(0, 4)

    family.classes = CLASSES.filter(c => c.id.substring(0, 4) == familyCode)

    res.json(family)
  }

  static getClassById(req, res) {
    const { id } = req.params 

    const uclass = CLASSES.find(c => c.id == id)

    if (!uclass) {
      return res.status(400).json({ error: `No class found for id ${id}` })
    }

    const classCode = id.substring(0, 6)

    uclass.commodities = COMMODITIES.filter(c => c.id.substring(0, 6) == classCode)

    res.json(uclass)
  }

  static search(req, res) {
    const { code, name } = req.query

    res.status(500).json({ error: `Unimplemented` })
  }
}

app.get('/segments', UNSPSCController.getSegments)
app.get('/segments/:id', UNSPSCController.getSegmentById)
app.get('/families/:id', UNSPSCController.getFamilyById)
app.get('/classes/:id', UNSPSCController.getClassById)
app.get('/search', UNSPSCController.search)

const PORT = process.env.PORT || 3005

app.listen(PORT)
